import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {13, 16, 782, 987, 101, 501, 2};

        System.out.println (Arrays.toString(array));

        ByCondition evenNumberFn = (digit) -> {
            if ((digit % 2) == 0) {
                return true;
            }
            return false;
        };

        int[] c = Sequence.filter(array, evenNumberFn);
        System.out.println(Arrays.toString(c));

        ByCondition evenSumFn = (number) -> {

            int sum = 0;
            while(number != 0){
                sum += (number % 10);
                number/=10;
            }

            if ((sum % 2) == 0)  {
                return true;
            }
            return false;
        };

        int[] d = Sequence.filter(array, evenSumFn);
        System.out.println(Arrays.toString(d));

       }
}




