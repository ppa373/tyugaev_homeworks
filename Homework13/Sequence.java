
public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int intermediateArrayIdx = 0;
        int[] intermediateResult = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                intermediateResult[intermediateArrayIdx++] = array[i];
            }
        }

        int[] result =  new int[intermediateArrayIdx];
        for (int i=0; i < intermediateArrayIdx; i++) {
            result[i] = intermediateResult[i];
        }
        return result;
    }
}


