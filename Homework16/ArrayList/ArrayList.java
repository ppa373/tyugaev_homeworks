package ArrayList;

import java.util.Arrays;

public class ArrayList<T> {
    private static final int DEFAULT_SIZE = 6;


    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[])new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    public void add(T element) {
          if (isFullArray()) {
            resize();
        }

        this.elements[size] = element;
        size++;
    }

    private void resize() {
        // запоминаем старый массив
        T[] oldElements = this.elements;
        // создаем новый массив, который в полтора раза больше предыдущего
        this.elements = (T[])new Object[oldElements.length + oldElements.length / 2];
        // копируем все элементы из старого массива в новый
        for (int i = 0; i < size; i++) {
            this.elements[i] = oldElements[i];
        }
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }


    public void removeAt(int index)  {
        if (index  < 0 || index > this.size) {
            return;
        }

        T[] clippedElements = (T[])new Object[this.size - 1];
        int clippedElementsIdx = 0;
        for (int i = 0; i < size; i++) {
            if (i != index) {
                clippedElements[clippedElementsIdx++] = elements[i];
            }
        }
        this.elements = clippedElements;
        this.size--;
    }


    public String toString() { return Arrays.toString(elements); }

}
