public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        // создаю новый узел
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);

        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    public T get(int index) {
        if (index < 0 || index >= size) {
            System.out.println("Invalid Index");
            return null;
        }

        Node<T> list = first;
        for (int i = 0; i < index; i++)
            list = list.next;

        return list.value;
    }

    public String toString() {
        LinkedList.Node<T> node = this.first;
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            sb = sb.append(node.value.toString() + " ");
            node = node.next;
        }
        sb = sb.append("]");
        return sb.toString();
    }
}
